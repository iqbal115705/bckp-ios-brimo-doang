Feature: Briva

  #	@Invalid
  #	Scenario Outline: User pay open briva bill - Abnormal Saldo
  #Given I start application
  #When I want login
  #When Abnormal Saldo - I try login with existing account <username> and <password>
  #Then I successfully go to dashboard
  #And I want transfer with virtual account with <condition> for <username>
  #When I saw my virtual account transfer history
  #When I want to add recipient of virtual account
  #When I try adding recipient of virtual account in condition with <brivaNumber>
  #Then Abnormal Saldo - I inputting <amount> for <whatPayment> Payment Briva with <username>
  #
  #Examples:
  #| username   | password   | pin 	 	| whatPayment	| brivaNumber						| amount	| condition	|
  #| bribri0001 | Jakarta123 | 123457 	| Open				| 2276808130852287			| 10000		|	NEW				|
  @Valid
  Scenario Outline: User pay open briva bill with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want transfer with virtual account with <condition> for <username>
    When I saw my virtual account transfer history
    When I want to add recipient of virtual account
    When I try adding recipient of virtual account in condition with <brivaNumber>
    When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
    When I confirm briva transaction with <detail>

    #When I validate my pin with <pin> before transaction
    #Then Transaction success
    Examples: 
      | username        | password   | pin    | whatPayment | brivaNumber       | amount | detail | decision | name    | debit          | condition |
      | denstoo02061997 | Jakarta123 | 123457 | Open        | 80777081931275981 |  10000 | briva  | Save     | Briva 1 | MUHAMAD TAUFAN | NEW       |

  #@Valid
  #Scenario Outline: User pay close briva bill with SA
  #Given I start application
  #When I want login
  #When I try login with existing account <username> and <password>
  #Then I successfully go to dashboard
  #And I want transfer with virtual account with <condition> for <username>
  #When I saw my virtual account transfer history
  #When I want to add recipient of virtual account
  #When I try adding recipient of virtual account in condition with <brivaNumber>
  #When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
  #When I confirm briva transaction with <detail>
  #When I validate my pin with <pin> before transaction
  #Then Transaction success
  #
  #Examples:
  #| username   | password   | pin 	 | whatPayment		| brivaNumber							| amount	| detail		| decision	| name	| debit					| condition	|
  #| bribri0001 | Jakarta123 | 123457 | Close					| 2737012345678924				| NULL		| briva			|	NULL			| NULL	| Rihana Elaela	| NULL			|
  #@Valid
  #Scenario Outline: User pay briva bill from list
  #Given I start application
  #When I want login
  #When I try login with existing account <username> and <password>
  #Then I successfully go to dashboard
  #And I want transfer with virtual account with <condition> for <username>
  #When I saw my virtual account transfer history
  #When I try pay briva from my list
  #When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
  #When I confirm briva transaction with <detail>
  #When I validate my pin with <pin> before transaction
  #Then Transaction success
  #
  #Examples:
  #| username   | password   | pin 	 | whatPayment	| amount	| detail		| decision	| name	| debit	| condition	|
  #| bribri0001 | Jakarta123 | 123457 | NULL					| 10000		| briva			| NULL			| Briva 1	| RATRI	| NULL		|
  #
  #@Valid
  #Scenario Outline: User pay briva bill from history
  #Given I start application
  #When I want login
  #When I try login with existing account <username> and <password>
  #Then I successfully go to dashboard
  #And I want transfer with virtual account with <condition> for <username>
  #When I saw my virtual account transfer history
  #When I try pay briva from my history
  #When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
  #When I confirm briva transaction with <detail>
  #When I validate my pin with <pin> before transaction
  #Then Transaction success
  #
  #Examples:
  #| username   | password   | pin 	 | whatPayment	| amount	| detail		| decision	| name	| debit	| contion	|
  #| bribri0001 | Jakarta123 | 123457 | NULL					| 10000		| briva			| NULL			| Briva 1	| RATRI	| NULL	|
  
  #@prod
  #Scenario Outline: User pay open briva bill with CA
    #Given I start application
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I also transfer with virtual account
    #When I saw my virtual account transfer history
    #When I want to add recipient of virtual account
    #When I try adding recipient of virtual account in condition with <brivaNumber>
    #When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
    #When I confirm briva transaction with <detail>
    #When I validate my pin with <pin> before transaction
    #Then Check transaction success
#
    #Examples: 
      #| username      | password    | pin    | whatPayment | brivaNumber     | amount | detail | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Open        | 999990000000012 |  10000 | briva  | null     | null | giro iqbal |

  #@prodXLS
  #Scenario Outline: User pay open briva bill with SA
    #Given I start application
    #When I get data from excel for <excel> and <sheet>
    #When I want login
    #When I try login with existing account <username> and <password>
    #Then I successfully go to dashboard
    #And I also transfer with virtual account
    #When I saw my virtual account transfer history
    #When I want to add recipient of virtual account
    #When I try adding recipient of virtual account in condition with <brivaNumber>
    #When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
    #When I confirm briva transaction with <detail>
    #When I validate my pin with <pin> before transaction
    #Then Check transaction success
#
    #Examples: 
      #| username      | password    | pin    | whatPayment | brivaNumber     | amount | detail | decision | name | debit      | excel | sheet |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Open        | 999990000000012 |  10000 | briva  | null     | null | giro iqbal | file  | briva |

  @Prod
  Scenario Outline: User pay open briva bill with SA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also transfer with virtual account
    When I want to add recipient of virtual account
    When I try adding recipient of virtual account
    When I inputting amount for briva payment
    When I confirm briva transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |
      
   @ProdNoLogin
  Scenario Outline: User pay open briva bill with SA
    Given Case <case>
    And I also transfer with virtual account
    When I want to add recipient of virtual account
    When I try adding recipient of virtual account
    When I inputting amount for briva payment
    When I confirm briva transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  @Briva_salah_pin1x
  Scenario Outline: User pay open briva bill with CA
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I also transfer with virtual account
    When I saw my virtual account transfer history
    When I want to add recipient of virtual account
    When I try adding recipient of virtual account in condition with <brivaNumber>
    When I inputting <amount> for <whatPayment> Payment Briva and <decision> with <name> , then choose account <debit>
    When I confirm briva transaction with <detail>
    When I input invalid pin
    And I try input pin again

    #When I validate my pin with <pin> before transaction
    #Then Check transaction success
    #When I validate my pin with <pin> before transaction
    #Then Transaction success
    Examples: 
      | username      | password   | pin    | whatPayment | brivaNumber     | amount | detail | decision | name | debit          | invalid_pin |
      | iqbalsugandhi | Ragunan511 | 123457 | Open        | 128085559547821 |  10000 | briva  | null     | null | @sugandhiiqbal |      111111 |
