Feature: Title of your feature
  I want to use this template for my feature file

  @prod
  Scenario Outline: User top up wallet from Top Up Baru
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And Click Dompet Digital
    When I want to add recipient of wallet
    When I try adding recipient of wallet
    When I inputting the top up amount
    When I confirm top up wallet
    When I validate my pin before transaction
    Then I successful go to bukti transaksi page
    Then Check transaction success

    Examples: 
      | case |
      |    6 |

  @prodNoLogin
  Scenario Outline: User top up wallet from Top Up Baru
    Given Case <case>
    And Click Dompet Digital
    When I want to add recipient of wallet
    When I try adding recipient of wallet
    When I inputting the top up amount
    When I confirm top up wallet
    When I validate my pin before transaction
    Then I successful go to bukti transaksi page
    Then Check transaction success

    Examples: 
      | case |
      |    6 |

  @ViaFastMenuList
  Scenario Outline: 
    Given Case <case>
    Given I start application
    When I want to wallet from fast menu
    When I top up wallet from my list
    When I inputting the top up amount
    When I confirm top up wallet
    When I validate my pin before transaction
    Then Check transaction success
    And I close application

    Examples: 
      | case |
      |    6 |
