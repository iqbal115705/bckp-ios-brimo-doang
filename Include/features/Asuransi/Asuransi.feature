#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Title of your feature
  I want to use this template for my feature file

  @prod
  Scenario Outline: User pay open briva bill with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    #When I saw my insurance bill history
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username   | password   | pin    | condition | detail                   | insuranceType | paymentType   | insuranceNumber | amountInsurance | whatPayment | decision | name           | debit |
      #| bribri0002 | Jakarta123 | 123457 | NULL      | Prudential Premi Pertama | Prudential    | Premi Pertama |        00900043 |           25000 | Open        | Save     | PPremi Pertama | RATRI |
      | case |
      |    1 |
      |    2 |
      |    3 |
      |    4 |
      |    5 |
      |    6 |
      |    7 |

  