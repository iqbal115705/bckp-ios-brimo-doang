@tag
Feature: Asuransi BRINS
  I want to use this template for my feature file
  
   @BRINS
  Scenario Outline: User pay open briva bill with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username   | password   | pin    | condition | detail                   | insuranceType | paymentType   | insuranceNumber | amountInsurance | whatPayment | decision | name           | debit |
      #| bribri0002 | Jakarta123 | 123457 | NULL      | Prudential Premi Pertama | Prudential    | Premi Pertama |        00900043 |           25000 | Open        | Save     | PPremi Pertama | RATRI |
      | case |
      |    1 |

  @BRINSNoLogin
  Scenario Outline: User pay open briva bill with CA
    Given Case <case>
    And I want to pay insurance bill with condition no DB
    When I want to add new insurance bill
    When I add new insurance bill with payment and number
    When I input amount for type payment insurance bill and decide , then choose fund
    When I confirm bill insurance transaction with detail
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |