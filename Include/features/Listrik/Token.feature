Feature: Token Listrik
  I want to test feature Listrik product Token with automation

  @EndToEnd
  Scenario Outline: User pay token electricity bill
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    1 |

  @EndToEndNoLogin
  Scenario Outline: User pay token electricity bill
    Given Case <case>
    And I also pay electricity bill
    When I saw my payment of electricity history
    When I want to add payment bill of electricity
    When I input payment bill of electricity
    When I add amount for bill of electricity and decision with name , then choose fund
    When I confirm payment electricity transaction
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      #| username      | password    | pin    | listrikType | listrikNumber | amountListrik | detail  | decision | name | debit      |
      #| iqbalsugandhi | Jangkrik511 | 112233 | Tagihan     |  547101010947 |         20000 | listrik | NULL     | NULL | giro iqbal |
      | case |
      |    1 |
