@tag
Feature: Detail Promo

  @tag1
  Scenario Outline: User check detail promo
    Given I start application
    Given ganti wifi
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to check detail promo
    Then I saw detail promo

    Examples: 
      | username   | password   | pin    |
      | bribri0001 | Jakarta123 | 123457 |

  @detailPromo
  Scenario Outline: User check detail promo
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to check detail promo
    Then I saw detail promo

    Examples: 
      | case |
      |    1 |

  @detailPromoNoLogin
  Scenario Outline: User check detail promo
    Given Case <case>
    And I want to check detail promo
    Then I saw detail promo

    Examples: 
      | case |
      |    1 |

  @detailPromoFM
  Scenario Outline: User check detail promo from fast menu
    Given Case <case>
    Given I start application
    And I want to check detail promo from fast menu
    Then I saw detail promo from fast menu

    Examples: 
      | case |
      |    1 |
