Feature: Donation


  @DompetDhuafa
  Scenario Outline: User do donate some their money
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I want to donate
    When I choose donation for purpose with ampunt and total
    When I choose fund account for donation
    When I confirm my donation
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |
      |    4 |

  @DompetDhuafaNoLogin
  Scenario Outline: User do donate some their money
    Given Case <case>
    And I want to donate
    When I choose donation for purpose with ampunt and total
    When I choose fund account for donation
    When I confirm my donation
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |
      |    4 |
      
  @DompetDhuafaZakatNoLogin
  Scenario Outline: User do donate some their money
    Given Case <case>
    And I want to donate
    When I choose donation for purpose with ampunt and total
    When I choose fund account for donation
    When I confirm my donation
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    3 |

      
    @DompetDhuafaInfaqNoLogin
  Scenario Outline: User do donate some their money
    Given Case <case>
    And I want to donate
    When I choose donation for purpose with ampunt and total
    When I choose fund account for donation
    When I confirm my donation
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    4 |