Feature: Paket Data

  #@Invalid
  #Scenario Outline: User buy data package (Ooredoo) with Empty Balance - Abnormal
  #Given I start application
  #When I want login
  #When Abnormal Saldo - I try login with existing account <username> and <password>
  #Then I successfully go to dashboard
  #And I want to buy pulsa with <condition> for <username>
  #When I choose data package and write my <numberPhone> phone and <amountPaketData>
  #Then Abnormal Saldo - I choose account debit for data package with <username>
  #
  #Examples:
  #| username   | password   | pin    | numberPhone | amountPaketData | condition | debit | provider |
  #| bribri0001 | Jakarta123 | 123457 | 08561234567 |           30000 | NEW       | RATRI | Ooredoo  |
  @Valid
  Scenario Outline: User buy data package (Ooredoo) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill

    #When I validate my pin before transaction
    #Then Transaction success
    Examples: 
      | case |
      |    7 |
      |    8 |

  #|   11 |
  #|   12 |
  #|   13 |
  @ViaFastMenu
  Scenario Outline: User buy data package via fast menu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |
      |    8 |

  #|   11 |
  #|   12 |
  #|   13 |
  
   @FMIndosat
  Scenario Outline: User buy data package via fast menu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |
      
   @FMTelkomsel
  Scenario Outline: User buy data package via fast menu
    Given Case <case>
    Given I start application
    And I want to buy pulsa from fast menu
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |
  
  @dataIndosat
  Scenario Outline: User buy data package (Ooredoo) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |

  @dataIndosatNoLogin
  Scenario Outline: User buy data package (Ooredoo) with CA
    Given Case <case>
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    7 |

  @dataTelkomsel
  Scenario Outline: User buy data package (telkomsel) with CA
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    8 |

  @dataTelkomselNoLogin
  Scenario Outline: User buy data package (telkomsel) with CA
    Given Case <case>
    And I buy pulsa or data
    When I choose data package and write my phone
    When I choose account for data package
    When I confirm data package bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    8 |
