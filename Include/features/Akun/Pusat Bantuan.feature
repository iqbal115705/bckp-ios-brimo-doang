Feature: Pusat Bantuan
  @tag1
  Scenario Outline: User check Help
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want use account feature
    And I need help about bri
    Then I found <title> of question about bri what i need
    Examples: 
      | username   | password   | title										|
      | taufan123456 | Jakarta123 | Apa itu aplikasi BRImo?	|