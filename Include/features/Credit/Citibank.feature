Feature: Credit

  @prodCitiBank
  Scenario Outline: User pay credit bill other bank
    Given Case <case>
    Given I start application
    When I want login
    When I try login with existing account
    Then I successfully go to dashboard
    And I suppose to pay credit bill
    When I want to add recipient of credit bill
    When I try adding recipient bank and number of credit bill condition
    When I input total and amount for the credit amount and decision with name , then choose account
    When I confirm payment of credit bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |

  #| username      | password    | pin    | creditBank    | creditNumber     | totalInput | amountCredit | detail       | decision | name | debit      |
  #| iqbalsugandhi | Jangkrik511 | 112233 | Citibank Card | 4541111123333315 | Manual     |        10000 | kartu kredit | NULL     | NULL | giro iqbal |
  @prodCitiBankNoLogin
  Scenario Outline: User pay credit bill other bank
    Given Case <case>
    And I suppose to pay credit bill
    When I want to add recipient of credit bill
    When I try adding recipient bank and number of credit bill condition
    When I input total and amount for the credit amount and decision with name , then choose account
    When I confirm payment of credit bill
    When I validate my pin before transaction
    Then Check transaction success

    Examples: 
      | case |
      |    2 |
