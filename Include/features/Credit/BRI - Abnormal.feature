Feature: Credit BRI

  Scenario Outline: User pay credit bill
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to pay credit bill
    When Abnormal <creditBank>, <creditNumber>, <totalInput>, <amount>
    When I confirm payment of credit bill with <detail>
    When I validate my pin with <pin> before transaction
    Then Transaction success

    Examples: 
      | username   | password   | pin 	 | creditBank	| creditNumber 			| totalInput	| amount	| detail				|
      | bribri0001 | Jakarta123 | 123457 | BRI				| 4359650100001206	| Manual			| 10000		| kartu kredit	|