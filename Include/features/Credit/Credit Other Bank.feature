Feature: Credit BRI

  Scenario Outline: User pay credit bill
    Given I start application
    When I want login
    When I try login with existing account <username> and <password>
    Then I successfully go to dashboard
    And I want to pay credit bill
    When I saw payment of credit bills history
    When I want to add recipient of credit bill
    When I try adding recipient of credit bill condition with <creditBank> and <creditNumber>
    When I inputting <amount> for the credit amount
    When I confirm payment of credit bill with <detail>
    When I validate my pin with <pin> before transaction
    Then Transaction success

    Examples: 
      | username   | password   | pin 	 | creditBank								| creditNumber 			| amount	| detail				|
      | bribri0001 | Jakarta123 | 123457 | Standard Chartered Card	| 1234567890123456	| 10000		| kartu kredit	|
      | bribri0001 | Jakarta123 | 123457 | ANZ Panin								| 1234567890123456	| 10000		| kartu kredit	|
      | bribri0001 | Jakarta123 | 123457 | HSBC Card								| 1234567890123456	| 10000		| kartu kredit	|
      | bribri0001 | Jakarta123 | 123457 | DBS Card									| 1234567890123456	| 10000		| kartu kredit	|