import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.Before

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject

import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile

import internal.GlobalVariable as GlobalVariable

import com.kms.katalon.core.annotation.BeforeTestCase
import com.kms.katalon.core.annotation.BeforeTestSuite
import com.kms.katalon.core.annotation.AfterTestCase
import com.kms.katalon.core.annotation.AfterTestSuite
import com.kms.katalon.core.context.TestCaseContext
import com.kms.katalon.core.context.TestSuiteContext

class ErrorHandling {
	/**
	 * Executes after every test case ends.
	 * @param testCaseContext related information of the executed test case.
	 */

	@BeforeTestSuite
	def sampleBeforeTestSuite(TestSuiteContext testSuiteContext) {
		GlobalVariable.currentTestSuitesID = testSuiteContext.getTestSuiteId()
	}

	@BeforeTestCase
	def sampleBeforeTestCase(TestCaseContext testCaseContext) {
		GlobalVariable.currentTestCaseId = testCaseContext.getTestCaseId()
	}

	@AfterTestCase
	def sampleAfterTestCase(TestCaseContext testCaseContext) {
		if (GlobalVariable.reportStatus != 'success') {
			
			Mobile.comment(GlobalVariable.reportStatus.toString())
			Scanner sc = new Scanner(new File("report_exported.csv"))
			String strNumber
			while(sc.hasNext()) {
				String filePerLine = sc.nextLine()
				String[] data = filePerLine.split(",")
				strNumber = data[0].trim ();
			}
			int number = 1
			FileWriter writer = new FileWriter("report_exported.csv",true);
			if (strNumber != null) {
				number = Integer.parseInt(strNumber)
				number = number + 1
			}

			Date today = new Date()

			String todaysDate = today.format('MM_dd_yy')

			String nowTime = today.format('hh_mm_ss')

			String formatDate = todaysDate + '-' + nowTime

			writer.write(number + "," +"null" +","+"null"+","+"null"+", "+formatDate+","+"null"+","+"null"+","+"NOT OK"+","+GlobalVariable.currentTestCaseId+"|")
			writer.write("\n")
			sc.close()
			writer.close();
		}

//		Mobile.delay(3)

		//String TSID = GlobalVariable.currentTestSuitesID
		
		String TSID = GlobalVariable.currentTestCaseId
		
		Mobile.comment(TSID)
		
		String lowerID = TSID.toLowerCase()

		if(lowerID.contains('no login')) {
			
			if(Mobile.verifyElementVisible(findTestObject('General/Back/XCUIElementTypeButton - BAIKLAH )'), 5, FailureHandling.OPTIONAL) == true) {
				Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - BAIKLAH )'), 5)
			}
			

			if (Mobile.verifyElementVisible(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton PIN 1'),
			5, FailureHandling.OPTIONAL) == true) {

				Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton PIN 1'), 5, FailureHandling.CONTINUE_ON_FAILURE)

				if (Mobile.verifyElementVisible(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton PIN 1'),
				5, FailureHandling.OPTIONAL) == true) {
					Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton PIN 1'), 5, FailureHandling.CONTINUE_ON_FAILURE)
				}
				Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - Ya'), 5, FailureHandling.CONTINUE_ON_FAILURE)
			}

			while (Mobile.verifyElementVisible(findTestObject('Feature Dashboard Home/XCUIElementTypeButton - Back'),
			5, FailureHandling.OPTIONAL) == true) {
				Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeButton - Back'), 5, FailureHandling.CONTINUE_ON_FAILURE)
			}
			
			GlobalVariable.reportStatus = 'running'
			
		}
	}
}