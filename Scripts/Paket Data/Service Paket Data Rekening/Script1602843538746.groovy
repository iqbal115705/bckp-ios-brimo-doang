import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords
import internal.GlobalVariable as GlobalVariable

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def common = ExcelKeywords.getExcelSheet(workBook, 'Common')

String text = ExcelKeywords.getCellValueByAddress(common, 'D2')

Mobile.verifyElementExist(findTestObject('Paket Data Rekening/XCUIElementTypeOther - PulsaData'), 0)

Mobile.verifyElementExist(findTestObject('Paket Data Rekening/XCUIElementTypeStaticText - Sumber Dana'), 0)

Mobile.tap(findTestObject('Paket Data Rekening/XCUIElementTypeImage - BrimoIconArrowDownOutline'), 0)

if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
    0, FailureHandling.OPTIONAL) == true) {
    Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)

    Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)
} else {
    Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

    Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)

    Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$text"]), 
        0)
}

Mobile.tap(findTestObject('Paket Data Rekening/XCUIElementTypeButton - Beli'), 0)

