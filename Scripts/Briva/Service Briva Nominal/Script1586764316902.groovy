import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.detroitlabs.katalonmobileutil.touch.Swipe as Swipe
import com.detroitlabs.katalonmobileutil.touch.Swipe.SwipeDirection as SwipeDirection
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Briva")

String amountBriva = ExcelKeywords.getCellValueByAddress(sheet1, 'C'+GlobalVariable.rowNumber)

def common = ExcelKeywords.getExcelSheet(workBook,"Common")

String debit = ExcelKeywords.getCellValueByAddress(common, 'D2')

String saveName = ExcelKeywords.getCellValueByAddress(sheet1, 'B'+GlobalVariable.rowNumber)

String whatPayment = ""

if (Mobile.verifyElementExist(findTestObject('Briva Open Payment Service/XCUIElementTypeStaticText - Nomor Tujuan'), 10, 
    FailureHandling.OPTIONAL) == true) {
    whatPayment = 'Open'
} else if (Mobile.verifyElementExist(findTestObject('Briva Close Payment Service/XCUIElementTypeStaticText - Total Pembayaran'), 
    10, FailureHandling.OPTIONAL) == true) {
    whatPayment = 'Close'
}

switch (whatPayment) {
    case 'Open':
        Mobile.verifyElementExist(findTestObject('Briva Open Payment Service/XCUIElementTypeOther - BRIVA'), 0)

        Mobile.setText(findTestObject('Briva Open Payment Service/XCUIElementTypeTextField - Nominal'), amountBriva, 
            0)

        Mobile.tap(findTestObject('Briva Open Payment Service/XCUIElementTypeStaticText - Nominal Bayar'), 0)

        Mobile.tap(findTestObject('Briva Open Payment Service/XCUIElementTypeImage - Pilih Rekening'), 0)

        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$debit"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        } else {
            Mobile.delay(5)

            Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        }
        
		CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Briva Open Payment Service/XCUIElementTypeButton - Bayar'), 0)

        break
    case 'Close':
        Mobile.verifyElementExist(findTestObject('Briva Close Payment Service/XCUIElementTypeStaticText - Total Pembayaran'), 
            0)

        Mobile.tap(findTestObject('Briva Close Payment Service/XCUIElementTypeImage - Pilih Rekening'), 0)

        if (Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                [('text') : "$debit"]), 0, FailureHandling.OPTIONAL) == true) {
            Mobile.delay(5)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        } else {
            Mobile.delay(5)

            Swipe.swipe(SwipeDirection.BOTTOM_TO_TOP)

            Mobile.verifyElementExist(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', 
                    [('text') : "$debit"]), 0)

            Mobile.tap(findTestObject('Transfer Nominal Form/XCUIElementTypeStaticText - Pilihan Rekening', [('text') : "$debit"]), 
                0)
        }
		
		if (saveName != 'null' && Mobile.verifyElementVisible(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0, FailureHandling.OPTIONAL) == true ) {
			Mobile.tap(findTestObject('Wallet Nominal Form/XCUIElementTypeButton - Simpan'), 0)
		
			Mobile.setText(findTestObject('Wallet Nominal Form/XCUIElementTypeTextField - Nama'), saveName, 0)
			
			String value = 'Nominal Tagihan'
			
			Mobile.tap(findTestObject('1 - Custom Object/Required StaticText and Value', [('value') : "$value"]), 0)
		}
		
		CustomKeywords.'screenshot.capture.Screenshot'()

        Mobile.tap(findTestObject('Briva Close Payment Service/XCUIElementTypeButton - Bayar'), 0)

        break
}

