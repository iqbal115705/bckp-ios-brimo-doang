import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Catatan Keuangan")

String category = ExcelKeywords.getCellValueByAddress(sheet1, 'D'+GlobalVariable.rowNumber)

if(Mobile.verifyElementNotVisible(findTestObject('PFM/XCUIElementTypeButton - Laporan'), 15, FailureHandling.OPTIONAL) == true) {
	Mobile.tap(findTestObject('PFM/detail pengeluaran/XCUIElementTypeButton - Back'), 0)
}

Mobile.tap(findTestObject('PFM/XCUIElementTypeButton - Laporan'), 0)

Mobile.verifyElementExist(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Laporan Keuangan'), 0)

categories = category.toString()

device_Height = Mobile.getDeviceHeight()
device_Width = Mobile.getDeviceWidth()
int startX = device_Width / 2
int endX = startX
int startY = device_Height * 0.70
int endY = device_Height * 0.30

if(Mobile.verifyElementNotVisible(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Kategori pengeluaran', [('cat'): "$categories"]), 0, FailureHandling.OPTIONAL) == true) {
	Mobile.swipe(startX, startY, endX, endY)
}

Mobile.verifyElementExist(findTestObject('PFM/Laporan/XCUIElementTypeStaticText - Kategori pengeluaran', [('cat'): "$categories"]), 0)

CustomKeywords.'screenshot.capture.Screenshot'()