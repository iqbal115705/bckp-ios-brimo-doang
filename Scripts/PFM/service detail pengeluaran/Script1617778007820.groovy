import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.keyword.excel.ExcelKeywords

def workBook = ExcelKeywords.getWorkbook("datasets/" + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook,"Catatan Keuangan")

String nominal = ExcelKeywords.getCellValueByAddress(sheet1, 'B'+GlobalVariable.rowNumber)

String categories = ExcelKeywords.getCellValueByAddress(sheet1, 'D'+GlobalVariable.rowNumber)

String notes = ExcelKeywords.getCellValueByAddress(sheet1, 'E'+GlobalVariable.rowNumber)

String payments = ExcelKeywords.getCellValueByAddress(sheet1, 'F'+GlobalVariable.rowNumber)

Mobile.tap(findTestObject('PFM/list catatan pengeluaran/XCUIElementTypeStaticText - output kategori pengeluaran', [('cat') : "$categories"]), 0)

Mobile.verifyElementExist(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - Detail Pengeluaran'), 0)

Mobile.verifyElementVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - kategori pengeluaran', [('cat') : "$categories"]), 0)

Mobile.verifyElementVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - ini catatan pengeluaran',[('notes') : "$notes"] ), 0)

String get_amount = Mobile.getText(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - output jumlah'), 0)

String filterAmount = get_amount.replaceAll("\\D+", "")

Mobile.verifyMatch(filterAmount, nominal, true, FailureHandling.STOP_ON_FAILURE)

date_detail_global = GlobalVariable.dateIndo

date_detail = Mobile.getAttribute(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - output tanggal'), 'value', 0)

Mobile.verifyMatch(date_detail, date_detail_global, false)

Mobile.verifyElementVisible(findTestObject('PFM/detail pengeluaran/XCUIElementTypeStaticText - output pembayaran', [('paid'): "$payments"]), 0)



