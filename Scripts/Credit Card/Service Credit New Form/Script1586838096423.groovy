import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.keyword.excel.ExcelKeywords as ExcelKeywords

def workBook = ExcelKeywords.getWorkbook('datasets/' + GlobalVariable.filePath)

def sheet1 = ExcelKeywords.getExcelSheet(workBook, 'Kartu Kredit')

String creditBank = ExcelKeywords.getCellValueByAddress(sheet1, 'A' + GlobalVariable.rowNumber)

String creditNumber = ExcelKeywords.getCellValueByAddress(sheet1, 'B' + GlobalVariable.rowNumber)

Mobile.verifyElementExist(findTestObject('Credit New Form/XCUIElementTypeStaticText - Bayar Kartu Kredit'), 0)

Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeButton - Pilih Bank'), 0)

switch (creditBank) {
    case 'BRI Card':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - BRI'), 0)

        break
    case 'Citibank Card':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - Citibank Card'), 0)

        break
    case 'ANZ Panin':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - ANZ Panin'), 0)

        break
    case 'HSBC Card':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - HSBC Card'), 0)

        break
    case 'DBS Card':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - DBS Card'), 0)

        break
    case 'Standard Chartered Card':
        Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - Standard Chartered Card (1)'), 0)

        break
    default:
        break
}

text = 'Nomor Kartu Kredit'

Mobile.setText(findTestObject('Credit New Form/XCUIElementTypeTextField - Nomor Kartu Kredit', [('text') : "$text"]), creditNumber.toString(), 
    0)

Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeStaticText - Bayar Kartu Kredit'), 0)

Mobile.tap(findTestObject('Credit New Form/XCUIElementTypeButton - Lanjutkan'), 0)

