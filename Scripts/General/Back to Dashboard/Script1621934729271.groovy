import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

while (Mobile.verifyElementNotVisible(findTestObject('Feature Dashboard Home/XCUIElementTypeStaticText - Saldo Rekening Utama'), 
    10, FailureHandling.OPTIONAL) == true) {
    if (Mobile.verifyElementVisible(findTestObject('Pin/XCUIElementTypeStaticText - Masukkan PIN (1)'), 10) == true) {
        Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton PIN 1'), 10)
    } else if (Mobile.verifyElementVisible(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton 1'), 10) == 
    true) {
        Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - BrimoBackButton 1'), 10)
    } else {
        Mobile.tap(findTestObject('Feature Dashboard Home/XCUIElementTypeButton - Back'), 10)
    }
    
    if (Mobile.verifyElementVisible(findTestObject('General/Back/XCUIElementTypeStaticText - Pembatalan Transaksi'), 10, 
        FailureHandling.OPTIONAL) == true) {
        Mobile.tap(findTestObject('General/Back/XCUIElementTypeButton - Ya'), 10)
    }
}