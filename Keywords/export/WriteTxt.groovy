package export
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import com.kms.katalon.core.configuration.RunConfiguration

import java.util.Scanner;
import java.io.File;
import java.io.FileWriter
import java.io.IOException;


class WriteTxt {
	/**
	 * Check if element present in timeout
	 * @param to Katalon test object
	 * @param timeout time to wait for element to show up
	 * @return true if element present, otherwise false
	 */

	//	private String path;
	//	private boolean append_to_file = false;
	//
	//	String filePath = RunConfiguration.getProjectDir()
	//
	//	//Writeover file
	//	public WriteTxt (String file_name) {
	//		path = filePath+'/Output/'+file_name;
	//	}
	//
	//	//Appends to file
	//	public WriteTxt (String file_name, boolean append_value) {
	//		path = filePath+'/Output/'+file_name;
	//		append_to_file = append_value;
	//	}
	//
	//	public void WriteToTxt (String file_name, String fitur, String time, String refnum, String nominal, String fee) throws IOException {
	//		FileWriter write = new FileWriter(path, append_to_file);
	//		PrintWriter print_line = new PrintWriter(write);
	//		print_line.printf(file_name+","+fitur +","+ GlobalVariable.accountNumber +","+time+","+refnum+","+nominal+","+fee);
	//		print_line.close();
	//	}

	//No|Fitur|No Rek Sumber|Tanggal|Reference|Nominal|Fee

	@Keyword
	def fileWrite(String fitur, String time, String refnum, String nominal, String fee) {
		Scanner sc = new Scanner(new File('report_exported.csv'))
		String strNumber
		while(sc.hasNext()) {
			String filePerLine = sc.nextLine()
			String[] data = filePerLine.split(",")
			strNumber = data[0].trim();
		}
		int number = 1
		FileWriter writer = new FileWriter('report_exported.csv' , true);
		if (strNumber != null) {
			writer.write("\n")
			number = Integer.parseInt(strNumber)
			number = number + 1
		}
		writer.write(number + "," +fitur +","+ GlobalVariable.accountNumber +","+time+","+refnum+","+nominal+","+fee+" | ");
		sc.close()
		writer.close();
	}
}