<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>via fast menu</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>10</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d635ea06-1465-4fc2-8249-fe997af65e95</testSuiteGuid>
   <testCaseLink>
      <guid>a5384fcf-acf0-453c-85ca-2cc7eff0c93e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/BRIZZI/Brizzi</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>91b6570b-dc6f-4b09-8f13-191a2ce1aebc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List ShopeePay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>accf0357-8a9f-4a51-a816-b6e8f67622b3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List Gopay</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ba1b765-fc8c-4157-87e0-046e9c8baa58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List OVO</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>893f211f-5cd2-4333-8534-6f4d2dbb710a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List Dana</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>73c8839e-74a8-497c-abc2-47d631480e75</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Dompet Digital/Via Fast Menu List LinkAja</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20ca0621-fecc-4a1b-bd55-b58ca67d4835</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Kode QR/Kode QR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b95978e-8171-472f-b014-d761f081956a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Three</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78c06c32-81a0-4fad-838d-ae69f2dddda1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Axis</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dd749257-7a1a-4a73-89d6-d00ae95b06f7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Smartfren</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de47f2df-8329-4ef6-8657-0ba9f9649ff8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>93baa29e-b913-4829-8ac6-da910299ba3f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7852aff-55c2-4876-8568-80544e2301e8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa/XL</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e82940e-d83a-460e-b8e7-9dbec3d372d8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa Data/Indosat</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b5e8626c-dc05-434f-9544-da34ec648130</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Pulsa Data/Telkomsel</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>953a193c-53eb-4968-a583-a0c52b91fa2a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/Promo/Detail Promo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>41ecb315-194b-4369-a2bc-b2c1a598af0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/PFM/Add Catatan Pemasukan Baru</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e4ecc58-0859-4f63-b5f9-740bffeda189</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/5 - Cucumber Fast Menu/PFM/Add Catatan Pengeluaran Baru</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
